class MessageSystem:

    def __init__(self):
        self.messageQueue = [];


    def AddRobot(self, robot):
        self.robot = robot;

    def update(self):
        if len(self.messageQueue) > 0:
            message = self.messageQueue.pop()
            self.robot.robotEvent(message)

    def sendMessage(self, _event):
        self.messageQueue.append(_event);


    #def Update(self):

