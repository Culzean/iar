import sys
import time

import Robot
import RobotEvent
import RobotBehaviour
import MessageSystem
import Vec3
import NavMesh
import common
import pygame


class session():

    def __init__(self, argv):

        if len(argv) > 0:
            self.outputFileName = argv[0]
        else:
            self.outputFileName = 'default.png'

        self.outputFileName = 'map_plots/' + self.outputFileName
        if (raw_input('Calibrate the IR sensors (y/n)? ') == 'y'):
            calibrate = True
        else:
            calibrate = False

        #set up the display
        pygame.init()
        self.messageSystem = MessageSystem.MessageSystem()
        self.robot = Robot.robot(self.messageSystem, self.outputFileName,
                                 calibrate=calibrate)
        self.messageSystem.AddRobot( self.robot)
        self.t = 0
        self.dt = 0
        self.start_time = time.time()
        self.prev_time = time.time()
        self.frame_time = 0.002
        start_message = RobotEvent.SessionStart()
        self.messageSystem.sendMessage(start_message)
        self.test_time = 5
        self.navMap = self.robot.navMesh
        self.DISPLAYSURF = pygame.display.set_mode((self.navMap.MAPWIDTH*self.navMap.TILESIZE,self.navMap.MAPHEIGHT*self.navMap.TILESIZE))

    def PollInput(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_q:
                    session.end()
                if event.key == pygame.K_f:
                    session.messageSystem.sendMessage(RobotEvent.FoodFound())

    def update(self):
        new_time = time.time()
        self.dt = new_time - self.prev_time
        self.prev_time = new_time

        self.PollInput()
        self.robot.update(self.dt, self.DISPLAYSURF)
        self.messageSystem.update()

        # Sleep for a bit to make it easier for PID
        # to compute derrivative and integral
        time.sleep(self.frame_time)

        #if (time.time() - self.start_time >= self.test_time and \
        #        not self.robot.going_home):
        #    message = RobotEvent.GoHomeStart()
        #    self.messageSystem.sendMessage(message)

        self.navMap.Draw(self.DISPLAYSURF)
        self.robot.Draw(self.DISPLAYSURF)

    def end(self):
        self.robot.navMesh.End()
        self.robot.setNewBehaviour(RobotBehaviour.Stop(self.messageSystem))
        self.robot.update(self.dt, self.DISPLAYSURF)
        self.messageSystem.update()

        self.robot.connection.Close()
        self.Running = False

if __name__ == '__main__':

    pygame.init()
    session = session(sys.argv[1:])
    session.Running = True

    while session.Running:
        session.update()
