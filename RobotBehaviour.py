import time
import Vec3
import numpy as np

import RobotEvent

class RobotBehaviour:

    def __init__(self):
        self.childNodes = []
        print "Hello Base"

    def update(self, _robot, dt):
        pass

    def printName(self):
        print(self.name)

    def addChild(self, _child):
        self.childNodes.append(_child)

    def start(self):
        pass

    def reset(self):
        pass

class Explore(RobotBehaviour):

    def __init__(self, message_system):
        self.childNode = []
        self.name = "Robot Explore"
        self.message_system = message_system
        self.walk_time = 0.332

    def update(self, _robot, dt):

        self.walk_time -= dt

        if (_robot.getIRSensorOutput() is not None):
            ir_output = _robot.getIRSensorOutput()
            print '(Explore) IR Output: ', ir_output
            front = np.sum(ir_output[_robot.S_FRONT])
            left = np.sum(ir_output[_robot.S_LEFT])
            right = np.sum(ir_output[_robot.S_RIGHT])





            speed = _robot.GO_SPEED

            # Robot is very close to object, change to avoid behaviour
            if (front > _robot.HTH_FRONT or left > _robot.HTH_LEFT or
                    right > _robot.HTH_RIGHT):
                message = RobotEvent.FoundObstacle()
                self.message_system.sendMessage(message)
                return

            elif self.walk_time <= 0:
                if _robot.crtGoal is not None:
                    message = RobotEvent.ExploreEnd()
                    self.message_system.sendMessage(message)
                elif len(_robot.navMesh.foodLocations) >= _robot.foodGatherThreshold and not _robot.going_home:
                    print "will not explore, Grezzer going to gather food"
                    message = RobotEvent.GatherFood()
                    self.message_system.sendMessage(message)
                elif _robot.going_home:
                    print "will not explore, Grezzer going home"
                    message = RobotEvent.GoHomeStart()
                    self.message_system.sendMessage(message)

            _robot.setSpeed(speed, speed)

        else:
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)

    def start(self):
        pass

    def reset(self):
        pass


class Stop(RobotBehaviour):

    def __init__(self, message_system):
        self.childNode = []
        self.name = "Robot stop"
        self.message_system = message_system

    def update(self,_robot, dt):
        _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)

class FoodFound(RobotBehaviour):

    def __init__(self, message_system, waitTime):
        self.childNode = []
        self.name = "Grezzer pick up food, yum! "
        self.message_system = message_system
        self.waitTime = waitTime

    def update(self,_robot, dt):

        if self.waitTime > 0:
            _robot.lightLED()
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)
            self.waitTime -= dt
            _robot.AddFoodSource(_robot.navMesh.ConvertPosToCell(Vec3.vec3(_robot.position.x, _robot.position.y, 0)))
            return
        else:
            if not _robot.isHeadingHome:
                print 'GO HOME '
                message = RobotEvent.GoHomeStart()
                self.message_system.sendMessage(message)
            else:
                pass

class ArriveHome(RobotBehaviour):

    def __init__(self, message_system, waitTime):
        self.childNode = []
        self.name = "Grezzer has got back home with food "
        self.message_system = message_system
        self.waitTime = waitTime
        self.robot = message_system.robot
        self.robot.isHeadingHome = False

    def update(self,_robot, dt):

        if self.waitTime > 0:
            _robot.lightLED()
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)
            self.waitTime -= dt
            return
        else:
            if _robot.SelectFoodSource() is not None:
                message = RobotEvent.GatherFood()
                self.message_system.sendMessage(message)
            else:
                message = RobotEvent.SearchForFood()
                self.message_system.sendMessage(message)


class Avoid(RobotBehaviour):

    def __init__(self, message_system):
        self.childNode = []
        self.name = "Robot Avoid Obstacle"
        self.message_system = message_system
        self.turn_dir = 0

    def start(self):
        self.turn_dir = 0

    def reset(self):
        self.turn_dir = 0

    def findSide(self, _robot, front, left, right):

            if _robot.crtPath is None or len(_robot.crtPath) <= 0:
                if left > right:
                    return _robot.RIGHT
                else:
                    return _robot.LEFT

            localGoal = _robot.crtPath[0]
            localGoal = _robot.navMesh.ConvertGridPosToMM(localGoal)
            vecToGoal = localGoal - _robot.position
            vecToGoal.normalize()

            rightturn = _robot.facingVec.perp_dot(vecToGoal)
            if rightturn > 0:
                return _robot.RIGHT
            else:
                return _robot.LEFT;

    def update(self,_robot, dt):

        if (_robot.getIRSensorOutput() is not None):
            ir_output = _robot.getIRSensorOutput()
            #print '(Avoid) IR Output: ', ir_output
            front = np.sum(ir_output[_robot.S_FRONT])
            left = np.sum(ir_output[_robot.S_LEFT])
            right = np.sum(ir_output[_robot.S_RIGHT])

            if (front > _robot.HTH_FRONT):
                # If something is detected in front, decide which way to turn
                side = self.findSide(_robot, front, left, right)
                if side == _robot.RIGHT or self.turn_dir == _robot.RIGHT_TURN:
                    self.turn_dir = _robot.RIGHT_TURN
                    _robot.setSpeed(2, -2)
                elif side == _robot.LEFT or self.turn_dir == _robot.LEFT_TURN:
                    self.turn_dir = _robot.LEFT_TURN
                    _robot.setSpeed(-2, 2)

            else:
                # Nothing detected in front
                if (left > _robot.HTH_LEFT or right > _robot.HTH_RIGHT):
                    # Wall detected left or right, start following wall
                    message = RobotEvent.FollowWallStart()
                    self.message_system.sendMessage( message )
                    return
                else:
                    message = RobotEvent.ExploreStart()
                    self.message_system.sendMessage( message )
                    return
                #    if (_robot.isHeadingHome):
                #        message = RobotEvent.GoHomeStart()
                #        self.message_system.sendMessage( message )
                #        return
                #    else:
                #        message = RobotEvent.ExploreStart()
                #        self.message_system.sendMessage( message )
                #        return
        else:
            # Stop robot if IR sensor output is wrong
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)


class FollowWallPID( RobotBehaviour ):
    def __init__(self, message_system):
        self.childNode = []
        self.name = "Robot follow wall using PID"
        self.message_system = message_system

        self.kp = 2/70.
        self.ki = 2/70.
        self.kd = 0/35.

        self.p_speed = 0
        self.pp_speed = 0

        self.integral = 0
        self.derivative = 0
        self.follow_side = 0

    def start(self):
        robot = self.message_system.robot
        if len(robot.crtPath) > 0:
            self.follow_side = self.findFollowSide(robot)
        else:
            self.follow_side = self.findFollowSideNoGoal(robot)


    def reset(self):
        self.follow_side = 0
        self.prev_error = 0
        self.integral = 0

    def findFollowSideNoGoal(self, _robot):

        if (_robot.getIRSensorOutput() is not None):
            ir_output = _robot.getIRSensorOutput()
            front = np.sum(ir_output[_robot.S_FRONT])
            left = np.sum(ir_output[_robot.S_LEFT])
            right = np.sum(ir_output[_robot.S_RIGHT])

            if (left > right):
                return _robot.LEFT
            else:
                return _robot.RIGHT
        else:
            self.findFollowSide(_robot)

    def findFollowSide(self, _robot):

        vecToGoal = _robot.crtGoal - _robot.position
        vecToGoal.normalize()

        if (_robot.getIRSensorOutput() is not None):
            rightturn = _robot.facingVec.perp_dot(vecToGoal)
            if rightturn:
                return _robot.RIGHT
            else:
                return _robot.LEFT;
        else:
            self.findFollowSide(_robot)

    def update(self, _robot, dt):

        if (_robot.getIRSensorOutput() is not None):
            ir_output = _robot.getIRSensorOutput()
            front = np.sum(ir_output[_robot.S_FRONT])
            left = np.sum(ir_output[_robot.S_LEFT])
            right = np.sum(ir_output[_robot.S_RIGHT])
            print '(FollowWallPID)IR Output: ', ir_output

            if (front > _robot.HTH_FRONT):
                message = RobotEvent.FoundObstacle()
                self.message_system.sendMessage(message)
                return

            elif (left < _robot.LTH_LEFT and right < _robot.LTH_RIGHT):
                if (_robot.going_home):
                    message = RobotEvent.GoHomeStart()
                    self.message_system.sendMessage( message )
                else:
                    message = RobotEvent.ExploreStart()
                    self.message_system.sendMessage( message )
                return

            if self.follow_side == _robot.LEFT:
                error = _robot.LEFT_AIM - left
            elif self.follow_side == _robot.RIGHT:
                error = _robot.RIGHT_AIM - right

            self.integral = self.integral + self.p_speed * dt
            self.derivative = (self.p_speed - self.pp_speed) / dt
            print 'Error:', error
            print 'Integral:', self.integral
            print 'Derivative:', self.derivative
            output = (self.kp * error) + (self.ki * self.integral) + \
                (self.kd * self.derivative)
            print 'Output:', output
            output = max(min(int(round(output)), 5), -5)

            self.pp_speed = self.p_speed
            self.p_speed = output
            print

            base_speed = 5

            if self.follow_side == _robot.LEFT:
                _robot.setSpeed(base_speed - output, base_speed + output)
            elif self.follow_side == _robot.RIGHT:
                _robot.setSpeed(base_speed + output, base_speed - output)

        else:
            # Stop robot if IR sensor output is wrong
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)


class GoHomeReactivePID(RobotBehaviour):

    def __init__(self, message_system):
        self.childNode = []
        self.name = "Robot Go Home Reactive with PID"

        self.message_system = message_system
        self.robot = message_system.robot
        self.robot.going_home = True
        self.home_pos = self.robot.navMesh.ConvertGridPosToMM( self.robot.homeLocation )
        self.home_dir = self.home_pos - self.robot.position
        self.home_dir.normalize()
        print "Home location ", self.home_dir
        self.home_dist = 0

        self.kp = 5.
        self.ki = 0/10.
        self.kd = 0/70.

        self.prevError = 0
        self.integral = 0
        self.derivative = 0
        #super(Avoid,self).__init__()

    def start(self):
        self.robot.turn_dir = 0

    def reset(self):
        self.robot.turn_dir = 0

    def sign(self, angle):
        #False is left, True is right
        return (angle < 0) if -1 else (angle > 0 if 1 else 0)

    def update(self, _robot, dt):

        print 'position: ', _robot.position
        self.home_dir = self.home_pos - self.robot.position
        self.home_dist = self.home_dir.distance()
        self.home_dir.normalize()
        self.facingVec = Vec3.vec3(np.cos(_robot.position.z),
                                   np.sin(_robot.position.z), 0)
        self.facingVec.normalize()

        angle = np.arccos((self.facingVec.x * self.home_dir.x + self.facingVec.y * self.home_dir.y) / (np.sqrt(self.home_dir.x**2 + self.home_dir.y**2) * np.sqrt(self.facingVec.x**2 + self.facingVec.y**2)))
        rightturn = self.facingVec.perp_dot(self.home_dir)

        self.integral = self.integral + angle * dt
        self.derivative = (angle - self.prevError) / dt
        output = (self.kp * angle) + (self.ki * self.integral) + \
                 (self.kd * self.derivative)
        #print output
        output = max(min(int(output), 7), 1)
        #print " output: " + str(output)
        self.prevError = angle


        #print 'Angle: ', angle

        if np.abs(self.home_dist) < 10:
            print "are are now at home"
            _robot.setSpeed(_robot.STOP_SPEED, _robot.STOP_SPEED)
        elif np.abs(angle) < (0.0007*self.home_dist):
            if (_robot.getIRSensorOutput() is not None):
                ir_output = _robot.getIRSensorOutput()
                front = np.sum(ir_output[_robot.S_FRONT])
                left = np.sum(ir_output[_robot.S_LEFT])
                right = np.sum(ir_output[_robot.S_RIGHT])

                if (front > _robot.HTH_FRONT or left > _robot.HTH_LEFT or
                        right > _robot.HTH_RIGHT):
                    message = RobotEvent.FoundObstacle()
                    self.message_system.sendMessage(message)
                    return

            _robot.setSpeed(_robot.GO_SPEED, _robot.GO_SPEED)
        else:
            if rightturn > 0:
                #right turn
                _robot.left_speed = -output
                _robot.right_speed = output
            else :
                #left turn
                _robot.left_speed = output
                _robot.right_speed = -output

class FollowPath(RobotBehaviour):

    def __init__(self, message_system, goal):
        self.childNode = []
        self.name = "Robot To follow path to goal"
        self.message_system = message_system
        self.robot = message_system.robot
        self.waypoint_dir = Vec3.vec3(0, 0, 0)
        self.waypoint_dist = 0

        self.kp = 5.
        self.ki = 0/10.
        self.kd = 0/70.

        self.prevError = 0
        self.integral = 0
        self.derivative = 0
        self.navMesh = self.robot.navMesh
        self.startCell = self.navMesh.ConvertPosToCell(self.robot.position)
        self.goal = goal
        self.path = self.robot.navMesh.AStar( self.startCell, goal )
        self.robot.crtPath = self.path
        self.robot.crtGoal = goal
        self.nextPoint = self.path.pop(0)
        self.nextWorldPoint = self.navMesh.ConvertGridPosToMM(self.nextPoint)
        #super(Avoid,self).__init__()
        print "Travelling to goal position ", self.goal
        print "Robot start position is ", self.robot.position
        print "Selecting first pos to travel to on grid ", self.nextPoint, " and point in world ", self.nextWorldPoint

    def start(self):
        self.robot.turn_dir = 0

    def reset(self):
        self.robot.turn_dir = 0

    def GotoPos(self, dt, pos):
        self.facingVec = Vec3.vec3(np.cos(self.robot.position.z),
                                   np.sin(self.robot.position.z), 0)
        self.facingVec.normalize()

        angle = np.arccos((self.facingVec.x * self.waypoint_dir.x + self.facingVec.y * self.waypoint_dir.y) / (np.sqrt(self.waypoint_dir.x**2 + self.waypoint_dir.y**2) * np.sqrt(self.facingVec.x**2 + self.facingVec.y**2)))
        rightturn = self.facingVec.perp_dot(self.waypoint_dir)

        self.integral = self.integral + angle * dt
        self.derivative = (angle - self.prevError) / dt
        output = (self.kp * angle) + (self.ki * self.integral) + \
                 (self.kd * self.derivative)
        output = max(min(int(output), 7), 1)
        # print " output: " + str(output)
        self.prevError = angle


        # print 'Angle: ', angle

        if np.abs(self.waypoint_dist) < 10:
            print "are have arrived at current way point ", pos
            self.robot.setSpeed(self.robot.STOP_SPEED, self.robot.STOP_SPEED)
        elif np.abs(angle) < (0.0007*self.waypoint_dist):
            if (self.robot.getIRSensorOutput() is not None):
                ir_output = self.robot.getIRSensorOutput()
                front = np.sum(ir_output[self.robot.S_FRONT])
                left = np.sum(ir_output[self.robot.S_LEFT])
                right = np.sum(ir_output[self.robot.S_RIGHT])

                if (front > self.robot.HTH_FRONT or left > self.robot.HTH_LEFT or
                        right > self.robot.HTH_RIGHT):
                    message = RobotEvent.FoundObstacle()
                    self.message_system.sendMessage(message)
                    return

            self.robot.setSpeed(self.robot.GO_SPEED, self.robot.GO_SPEED)
        else:
            if rightturn > 0:
                #right turn
                self.robot.left_speed = -output
                self.robot.right_speed = output
            else :
                #left turn
                self.robot.left_speed = output
                self.robot.right_speed = -output

    def update(self, _robot, dt):
        # print 'position: ', self.robot.position
        # print 'next world point: ', self.nextWorldPoint
        self.goal_dir = self.navMesh.ConvertGridPosToMM(self.goal) - self.robot.position
        self.goal_dist = self.goal_dir.distance()

        if self.goal_dist < 10:
            print "Path complete by dist"
            self.PathComplete()
            return

        self.waypoint_dir = self.nextWorldPoint - self.robot.position
        self.waypoint_dist = self.waypoint_dir.distance()
        self.waypoint_dir.normalize()

        if(self.waypoint_dist > 20):
            self.GotoPos(dt,self.nextWorldPoint)
        else:
            if len(self.path) <= 0:
                print "Path has no mode nodes!!!", self.goal, self.robot.position
                self.PathComplete()
            else:
                self.nextPoint = self.path.pop(0)
                self.nextWorldPoint = self.navMesh.ConvertGridPosToMM(self.nextPoint)
                self.GotoPos(dt, self.nextWorldPoint)
                print "Selecting next pos to travel to ", self.nextPoint

    def PathComplete(self):
        print "We have arrived at our destination ", self.goal_dist
        self.robot.crtPath = []
        self.robot.crtGoal = None
        if self.robot.isHeadingHome:
            message = RobotEvent.GoHomeEnd()
            self.message_system.sendMessage(message)
        else:
            message = RobotEvent.FoodFound()
            self.message_system.sendMessage( message )

    def start(self):
        pass

    def rest(self):
        print "Arrived at goal " + str(self.home_dist)


class AngleCalibrate(RobotBehaviour):

    def __init__(self, message_system):
        self.childNode = []
        self.message_system = message_system
        self.robot = message_system.robot
        self.turn_dir = 0
        self.name = " will calibrate angle"#
        self.startAngle = self.robot.position.z;
        self.rightTest = False
        self.rotSpeed = 10
        #super(Avoid,self).__init__()

    def start(self):
        self.robot.turn_dir = 0

    def reset(self):
        self.robot.turn_dir = 0

    def update(self, _robot, dt):

        if np.abs(self.startAngle - self.robot.position.z) >= 12.5664:
            print "Angle calibrate has finished"
            message = RobotEvent.SessionStop()
            self.message_system.sendMessage( message )
        else:
            if self.rightTest:
                _robot.setSpeed(self.rotSpeed, -self.rotSpeed)
            else:
                _robot.setSpeed(-self.rotSpeed, self.rotSpeed)
