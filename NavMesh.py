import RobotBehaviour
import RobotEvent
import Vec3
import time
import numpy as np
import matplotlib.pyplot as plt
import heapq
import random
import matplotlib.patches as Patch
import Queue
from numpy import genfromtxt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import pygame, sys
from pygame.locals import *

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['reverse_mapping'] = reverse
    return type('Enum', (), enums)

NodeType = enum('Clear', 'Home', 'Wall', 'Food')

class NavMesh():

    def __init__(self, cols, rows, inputfilename,outputfilename):
        self.outputfilename = outputfilename
        self.inputfilename = inputfilename
        self.cols = cols
        self.rows = rows
        self.mapDim = Vec3.vec3(self.cols,self.rows,0) #in mm arena is 140 * 77, scale a cell to 5mmby5mm
        self.map = self.LoadCSVMap(self.inputfilename, self.cols, self.rows)

        self.foodLocations = []

        self.BLACK = (0,   0,   0  )
        self.WHITE = (255, 255,  255)
        self.GREEN = (0,   255, 0  )
        self.BLUE  = (0,   0,   255)
        self.PURPLE = (186, 38, 174)
        self.CYAN = (0,255,255)
        self.GREY = (135, 150, 156)

        #constants representing the different resources
        self.EMPTY = 1
        self.EXPLORED = 2
        self.WOOD = 2579
        self.DARKWOOD = 2195
        self.STARTPOS = 1549
        self.GUTTER = 595
        self.CORNER = 768

        #a dictionary linking resources to colours
        self.colours =   {
            self.EMPTY  : self.WHITE,
            self.WOOD : self.BLACK,
            self.DARKWOOD : self.BLUE,
            self.STARTPOS : self.PURPLE,
            self.GUTTER : self.CYAN,
            self.CORNER : self.GREY
        }

        self.crtPath = [];

        #useful game dimensions
        self.CellSizeMM = 20
        self.TILESIZE  = 16
        self.MAPWIDTH  = self.cols
        self.MAPHEIGHT = self.rows

        self.ARENAWIDTH = 1440 #mm
        self.ARENAHEIGHT =780

        self.start_pos = (36,6)
        self.ends = [(x,25) for x in range(43,44)]
        self.valid = []





    def Draw(self, canvas):
        for end in self.ends:
            if self.isDirectPath(self.start_pos, end):
                self.valid.append(True)
            else:
                self.valid.append(False)
        canvas.fill((0,0,0))
        for row in range(self.MAPHEIGHT):
            rowAligned = self.rows - (row)
            #loop through each column in the row
            for column in range(self.MAPWIDTH):
                #draw the resource at that position in the tilemap, using the correct colour
                pygame.draw.rect(canvas, self.colours[self.map[column][row]], (column*self.TILESIZE,rowAligned*self.TILESIZE,self.TILESIZE,self.TILESIZE))

        for i, end in enumerate(self.ends):
            if self.valid[i]:
                pygame.draw.line(canvas, (0,255,0), self.ConvertGridPosToCanvas((self.start_pos[0],self.start_pos[1])), self.ConvertGridPosToCanvas((end[0], end[1])))
            else:
                pygame.draw.line(canvas, (255,0,0), self.ConvertGridPosToCanvas((self.start_pos[0],self.start_pos[1])), self.ConvertGridPosToCanvas((end[0], end[1])))


        for cell in self.crtPath:
            drawCell = self.ConvertGridPosToMM(cell)
            drawCell = self.ConvertMMToCanvas(drawCell)
            #print "Drawing path ", drawCell
            pygame.draw.rect(canvas, self.PURPLE,(drawCell[0],drawCell[1],self.TILESIZE,self.TILESIZE))
        for food in self.foodLocations:
            drawCell = self.ConvertGridPosToMM(food)
            drawCell = self.ConvertMMToCanvas(drawCell)
            pygame.draw.rect(canvas, self.GREEN, (drawCell[0],drawCell[1],self.TILESIZE,self.TILESIZE))

        pygame.display.update()


    def Update(self, _robot, dt):
        if (_robot.getIRSensorOutput()):
            front, left, right = _robot.getIRSensorOutput()

        if front > 10:
            distToObj = front * self.irTOmmRatio
            readingPos = _robot.pos
            readingPos += Vec3.vec3( np.cos(self.phi), np.sin(self.phi), 0 ) * distToObj

    def GetNode(self, pos):
        if pos[0] < 0 or pos[0] >= self.cols or pos[1] < 0 or pos[1] >= self.rows:
            return self.DARKWOOD
        else:
            return self.map[pos[0], pos[1]]

    def in_bounds(self, id):
            (x, y) = id
            return 0 <= x < self.cols and 0 <= y < self.rows

    def Neighbours(self, id):
            (x, y) = id
            results = [(x+1, y), (x, y-1), (x-1, y), (x, y+1)]
            if (x + y) % 2 == 0: results.reverse() # aesthetics
            results = filter(self.in_bounds, results)
            results = filter(self.IsAccessibleOffRoad, results)
            return results

    def CellCost(self, pos):
        cell = self.GetNode(pos)
        if cell == self.CORNER:
            return 2
        elif cell == self.GUTTER:
            return 4
        else:
            return 1

    def getClosestObj(self, dist1, dist2, cell_pos, dir1, dir2):
        if (dist1 < dist2):
            if not self.IsAccessible(cell_pos+dir1):
                return dist1, self.GetNode(cell_pos+dir1)
            elif not self.IsAccessible(cell_pos+dir1+dir2):
                return dist2, self.GetNode(cell_pos+dir1+dir2)
            else:
                return -1
        else:
            if not self.IsAccessible(cell_pos+dir2):
                return dist1, self.GetNode(cell_pos+dir2)
            elif not self.IsAccessible(cell_pos+dir1+dir2):
                return dist2, self.GetNode(cell_pos+dir1+dir2)
            else:
                return -1

    def isDirectPath(self, cell_pos1, cell_pos2):
        cell_pos1 = Vec3.vec3(cell_pos1[0], cell_pos1[1], 0)
        cell_pos2 = Vec3.vec3(cell_pos2[0], cell_pos2[1], 0)
        dx = cell_pos2[0]-cell_pos1[0]
        dy = cell_pos2[1]-cell_pos1[1]


        if dx == 0.0 and dy == 0.0:
            return True
        #print dx,dy
        if dy >= 0:
            angle = np.arctan2(dy, dx)
        else:
            angle = np.pi*2-np.arctan2(dy, dx)

        #print 'ANGLE: ', angle


        x_dir = Vec3.vec3(1,0,0)
        y_dir = Vec3.vec3(0,1,0)

        pos_in_cell = Vec3.vec3(10, 10, 0)


        if (angle == 0.0):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                if self.IsAccessible(cell_pos1+x_dir):
                    cell_pos1[0] += 1
                else:
                    return False
            return True

        if (angle > 0 and angle < 0.5*np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                right_dist = (20 - pos_in_cell.x) / np.cos(angle)
                top_dist = (20 - pos_in_cell.y) / np.cos(0.5*np.pi - angle)
                if (right_dist < top_dist):
                    if self.IsAccessible(cell_pos1+x_dir):
                        cell_pos1[0] += 1
                        newy = (20-pos_in_cell.x)*np.tan(angle)
                        pos_in_cell = Vec3.vec3(0, pos_in_cell.y+newy, 0)
                    else:
                        return False
                else:
                    if self.IsAccessible(cell_pos1+y_dir):
                        cell_pos1[1] += 1
                        newx = (20-pos_in_cell.y)*np.tan(0.5*np.pi - angle)
                        pos_in_cell = Vec3.vec3(pos_in_cell.x+newx, 0, 0)
                    else:
                        return False
            return True

        if (angle == 0.5*np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                if self.IsAccessible(cell_pos1+y_dir):
                    cell_pos1[1] += 1
                else:
                    return False
            return True

        elif (angle > 0.5*np.pi and angle < np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                left_dist = (pos_in_cell.x) / np.cos(np.pi - angle)
                top_dist = (20 - pos_in_cell.y) / np.cos(angle - 0.5*np.pi)
                if (left_dist < top_dist):
                    if self.IsAccessible(cell_pos1-x_dir):
                        cell_pos1[0] -= 1
                        newy = (pos_in_cell.x)*np.tan(np.pi-angle)
                        pos_in_cell = Vec3.vec3(20, pos_in_cell.y+newy, 0)
                    else:
                        return False
                else:
                    if self.IsAccessible(cell_pos1+y_dir):
                        cell_pos1[1] += 1
                        newx = (20-pos_in_cell.y)*np.tan(angle - 0.5*np.pi)
                        pos_in_cell = Vec3.vec3(pos_in_cell.x-newx, 0, 0)
                    else:
                        return False
            return True

        if (angle == np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                if self.IsAccessible(cell_pos1-x_dir):
                    cell_pos1[0] -= 1
                else:
                    return False
            return True

        elif (angle > np.pi and angle < 1.5*np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                left_dist = (pos_in_cell.x) / np.cos(angle - np.pi)
                bottom_dist = (pos_in_cell.y) / np.cos(1.5*np.pi - angle)
                if (left_dist < bottom_dist):
                    if self.IsAccessible(cell_pos1-x_dir):
                        cell_pos1[0] -= 1
                        newy = (pos_in_cell.x)*np.tan(angle-np.pi)
                        pos_in_cell = Vec3.vec3(20, pos_in_cell.y-newy, 0)
                    else:
                        return False
                else:
                    if self.IsAccessible(cell_pos1-y_dir):
                        cell_pos1[1] -= 1
                        newx = (pos_in_cell.y)*np.tan(1.5*np.pi - angle)
                        pos_in_cell = Vec3.vec3(pos_in_cell.x-newx, 20, 0)
                    else:
                        return False
            return True

        if (angle == 1.5*np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                if self.IsAccessible(cell_pos1-y_dir):
                    cell_pos1[1] -= 1
                else:
                    return False
            return True

        elif (angle > 1.5*np.pi and angle < 2*np.pi):
            while cell_pos1[0] != cell_pos2[0] or cell_pos1[1] != cell_pos2[1]:
                right_dist = (20 - pos_in_cell.x) / np.cos(2*np.pi - angle)
                bottom_dist = (pos_in_cell.y) / np.cos(angle - 1.5*np.pi)
                if (right_dist < bottom_dist):
                    if self.IsAccessible(cell_pos1+x_dir):
                        cell_pos1[0] += 1
                        newy = (20-pos_in_cell.x)*np.tan(2*np.pi-angle)
                        pos_in_cell = Vec3.vec3(0, pos_in_cell.y-newy, 0)
                    else:
                        return False
                else:
                    if self.IsAccessible(cell_pos1-y_dir):
                        cell_pos1[1] -= 1
                        newx = (pos_in_cell.y)*np.tan(angle - 1.5*np.pi)
                        pos_in_cell = Vec3.vec3(pos_in_cell.x+newx, 20, 0)
                    else:
                        return False
            return True

    def CostHeuristic(self, a, b):
        (x1, y1) = a
        (x2, y2) = b
        return (abs(x1 - x2) + abs(y1 - y2))

    def IsAccessible(self, pos):
        cell = self.GetNode(pos)
        return not (cell == self.WOOD or cell == self.DARKWOOD or cell == self.CORNER or cell == self.GUTTER)

    def IsAccessibleOffRoad(self, pos):
        cell = self.GetNode(pos)
        return not (cell == self.WOOD or cell == self.DARKWOOD)

    def AStar(self, start, goal):
        start = (start[0], start[1])
        goal = (goal[0], goal[1])
        # print "Searching for path starting at ", start, " arriving at ", goal
        openSet = Queue.PriorityQueue()
        openSet.put([start[0], start[1]], 0)
        closedSet = []
        cameFrom = {}
        cameFrom[start] = None

        #costG = np.full((self.cols, self.rows), 9999999)
        costG = {}
        costG[start] = 0
        costF = np.full((self.cols, self.rows), 9999999)
        costF[start[0], start[1]] = costG[start[0], start[1]] + self.CostHeuristic(start,goal)

        while not openSet.empty():
            current = openSet.get(True)
            if current == goal:
                # print cameFrom
                return self.ReconstructPath(cameFrom, start, goal)

            neighbours = self.Neighbours(current)

            for neighbour in neighbours:
                newCost = costG[current[0],current[1]] + self.CellCost(neighbour) #something of a simplification here, no travel heuristic
                if neighbour not in costG or newCost < costG[neighbour]:
                    #if neighbour not in openSet:
                    priority = newCost + self.CostHeuristic(neighbour,goal)
                    openSet.put( (neighbour[0],neighbour[1]), priority)

                    #then this path is very good
                    cameFrom[neighbour] = current
                    costG[neighbour[0],neighbour[1]] = newCost
                    costF[neighbour[0],neighbour[1]] = newCost + self.CellCost(neighbour)

        # print "No path has been found for this goal ", goal
        return [start]

    def ReconstructPath(self, cameFrom, start,goal):
        #total_path = np.array[goal]
        current = goal
        path = [current]
        while not ((current[0] is start[0]) and (current[1] is start[1])):
            current = cameFrom[current]
            #print "Path so far ", current
            path.append(current)
        #path = path[:len(path)-2]
        path.reverse()
        print path
        #print "Path to follow ", path
        start_i = 0
        new_path = []

        while start_i != len(path)-1:
            furthest_direct_i = 1
            new_path.append(path[start_i])
            for i, node in enumerate(path[start_i+1:]):
                if self.isDirectPath(path[start_i], node):
                    furthest_direct_i = i+1
                if i == len(path[start_i+1:])-1:
                    start_i += furthest_direct_i

        new_path.append(path[-1])

        path = new_path[1:]
        print '----------:', path
        self.crtPath = path
        return path

    def UpdateVal(self, pos, nodeType):
        # print "Node " + pos.tostring() + "has been updated to " + str(nodeType)
        self.map[pos.x, pos.y] = nodeType

    def PlotPathPoint(self, pos):
        self.vert += [(pos.x, pos.y)]
        self.path = np.array( self.vert, float)
        self.drawCodes += [Path.LINETO]
        self.drawPath = Path( self.path, self.drawCodes )
        self.pathPatch = PathPatch(self.drawPath, facecolor='None', edgecolor='green')

        self.ax.add_patch( self.qpathPatch )
        self.ax.autoscale_view()

    def ConvertPosToCell(self, pos):
        posX = round(pos[0] / self.CellSizeMM)
        posY = round(pos[1] / self.CellSizeMM)
        return Vec3.vec3(posX, posY, 0)

    def ConvertMMToCanvas(self, pos):
        posX = pos[0] * (self.cols * self.TILESIZE) / self.ARENAWIDTH
        posY = (self.rows * self.TILESIZE) - (pos[1] * (self.rows * self.TILESIZE) / self.ARENAHEIGHT)
        return Vec3.vec3(posX, posY, 0)

    def PlotPosInMM(self, pos, col, canvas):
        posX = pos[0] * (self.cols * self.TILESIZE) / self.ARENAWIDTH
        posY = (self.rows * self.TILESIZE) - (pos[1] * (self.rows * self.TILESIZE) / self.ARENAHEIGHT)
        cell =  Vec3.vec3(posX, posY, 0)
        halfWidth = self.TILESIZE / 2

        pygame.draw.rect(canvas, col, (cell[0] - halfWidth/2,cell[1] - halfWidth/2, halfWidth, halfWidth))
        pygame.display.update()

    def ConvertGridPosToCanvas(self, pos):
        res = self.ConvertMMToCanvas(self.ConvertGridPosToMM(pos))
        return (res[0], res[1])

    def ConvertGridPosToMM(self, pos):
        return Vec3.vec3( pos[0] * self.ARENAWIDTH / self.cols, pos[1] * self.ARENAHEIGHT / self.rows, 0 )

    def AddFoodSource(self, cellPoint):
        self.foodLocations.append(cellPoint)

    def End(self):
        plt.show()
        plt.savefig(self.outputfilename)
        plt.close()

    def LoadCSVMap(self, fname,cols, rows):
        ret = genfromtxt(fname, delimiter=',')
        ret = np.transpose(ret)
        ret = np.fliplr(ret)
        return ret
