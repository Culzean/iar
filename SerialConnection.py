import serial
import time
import sys
import glob
import numpy as np


class SerialConnection:

    def __init__(self):
        #print self.serial_ports()
        self.connection = serial.Serial(port="/dev/ttyS0", baudrate=9600, stopbits=2, timeout=1)
        self.connection.readlines()

    def SendCommand(self, command):
        if self.connection.isOpen():
            if self.connection.inWaiting() > 0 :
                while self.connection.inWaiting() > 0 :
                    self.connection.readline()[:-1]
            self.connection.write(command)
            self.connection.flush()
            time.sleep(0.002)

            output = self.connection.readline()

            # print 'Serial Output(Command=' + command.rstrip('\n') + '): ', output

            # Strip the newline and carriage return from every output
            output = output.rstrip('\r\n').replace("\x00", "")


            if output != '':
                return output

            return None

        else:
            self.connection = serial.Serial(0, 9600, timeout=0.1)
            self.connection.readlines()
            self.SendCommand(command)


    def Close(self):
        self.connection.close()

    def serial_ports(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result



# main for testing
if __name__ == '__main__':
    init_ir_output = np.zeros(8)
    connection = SerialConnection()

    i = 50.
    while (i > 0):
        output = connection.SendCommand("N\n")
        print output

        output = output[2:].split(',')
        print output
        init_ir_output += np.array(map(float, output))

        i -= 1

    init_ir_output *= 1/50.
    print init_ir_output

    while 1:
        output = connection.SendCommand("N\n")
        output = output[2:].split(',')
        if len(np.array(map(float, output))) == 8:
            output = np.array(map(float, output)) - init_ir_output
            # print 'Guess: ', np.around(157.9868139*(np.clip(np.average(output[2:4]), 0, np.inf)**(-0.4714736709)), 3)
            print 'Guess: ', np.around(321.7844113*(np.clip(np.average(output[2:4]), 0, np.inf)**(-0.4983706788)), 3)
            # print 'left: ', np.average(output[0:2]), ' | front: ', np.average(output[2:4]), ' | right: ', np.average(output[4:6])

