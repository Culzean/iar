import numpy as np
import pygame, sys
from pygame.locals import *
import bisect

import Vec3
import NavMesh


def add_noise(amount, *data):
    return [x+np.random.uniform(-amount,amount) for x in data]

sigma2 = 0.9 ** 2
def w_gauss(a, b):
    error = a - b
    g = np.e ** -(error ** 2 / (2 * sigma2))
    return g


class Particle(object):
    def __init__(self, x, y, angle, w=1):
        self.pos = Vec3.vec3(x, y, angle)
        self.w = w

class WeightedDistribution(object):
    def __init__(self, state):
        accum = 0.0
        self.state = [p for p in state if p.w > 0]
        self.distribution = []
        for x in self.state:
            accum += x.w
            self.distribution.append(accum)


    def pick(self):
        try:
            pick = self.state[bisect.bisect_left(self.distribution,
                                                 np.random.uniform(0, 1))]
            if pick is None:
                print state
            return pick
        except IndexError:
            # Happens when all particles are improbable w=0
            return None


class OdometryParticleFilter:

    def __init__(self, starting_pos, navmap):
        self.navmap = navmap
        self.num_particles = 100
        # We are certain about the starting position
        self.pos = starting_pos
        for i in range(self.num_particles):
            self.particles = [Particle(starting_pos.x, starting_pos.y,
                                       starting_pos.z) for _ in
                              range(self.num_particles)]

    def CalculateWeights(self):
        pass

    def ParticleSensorReading(self, particle):
        angles = []
        angles.append(particle.pos.z)
        angles.append(particle.pos.z - (45*np.pi / 180))
        angles.append(particle.pos.z + (45*np.pi / 180))

        # cell_pos = self.navmap.ConvertPosToCell(particle.pos)
        x_dir = Vec3.vec3(1,0,0)
        y_dir = Vec3.vec3(0,1,0)
        # pos_in_cell = Vec3.vec3(particle.pos.x % 40, particle.pos.y % 40, 0)

        res = []

        for angle in angles:
            angle = angle % 2*np.pi

            s_pos = particle.pos + (Vec3.vec3(30*np.cos(angle),
                                              30*np.sin(angle), 0))
            cell_pos = self.navmap.ConvertPosToCell(s_pos)
            pos_in_cell = Vec3.vec3(s_pos.x % 20, s_pos.y % 20, 0)

            if (angle > 0 and angle < 0.5*np.pi):
                right_dist = (20 - pos_in_cell.x) / np.cos(angle)
                top_dist = (20 - pos_in_cell.y) / np.cos(0.5*np.pi - angle)
                res.append(self.navmap.getClosestObj(right_dist, top_dist,
                                                     cell_pos, x_dir, y_dir))
            elif (angle > 0.5*np.pi and angle < np.pi):
                left_dist = (pos_in_cell.x) / np.cos(np.pi - angle)
                top_dist = (20 - pos_in_cell.y) / np.cos(angle - 0.5*np.pi)
                res.append(self.navmap.getClosestObj(left_dist, top_dist,
                                                     cell_pos, -x_dir, y_dir))

            elif (angle > np.pi and angle < 1.5*np.pi):
                left_dist = (pos_in_cell.x) / np.cos(angle - np.pi)
                bottom_dist = (pos_in_cell.y) / np.cos(1.5*np.pi - angle)
                res.append(self.navmap.getClosestObj(left_dist, bottom_dist,
                                                     cell_pos, -x_dir, -y_dir))

            elif (angle > 1.5*np.pi and angle < 2*np.pi):
                right_dist = (20 - pos_in_cell.x) / np.cos(2*np.pi - angle)
                bottom_dist = (pos_in_cell.y) / np.cos(angle - 1.5*np.pi)
                res.append(self.navmap.getClosestObj(right_dist, bottom_dist,
                                                     cell_pos, x_dir, -y_dir))

        return res



    def distToIr(self, dist, material):
        if material == self.navmap.DARKWOOD:
            return 46056.9/(dist**2.121009213708778)
            # return np.around(157.9868139*(ir**(-0.4714736709)), 3)
        else:
            return 107529./(dist**2.006538591732255)
            # return np.around(321.7844113*(ir**(-0.4983706788)), 3)

    def computeMean(self):
        sum_x, sum_y, sum_angle = 0, 0, 0
        for p in self.particles:
            sum_x += p.pos.x
            sum_y += p.pos.y
            sum_angle += p.pos.z
            res = Vec3.vec3(sum_x/self.num_particles, sum_y/self.num_particles,
                         sum_angle/self.num_particles)
        return res

    def update(self, dx, dy, dangle, s_front, s_left, s_right, canvas):

        for i, p in enumerate(self.particles):
            p.pos.x += add_noise(0.1, dx)
            p.pos.y += add_noise(0.1, dy)
            p.pos.z = (p.pos.z + add_noise(.0005, dangle)) % (2*np.pi)

            real_readings = [s_front, s_left, s_right]

            res = self.ParticleSensorReading(p)
            weight = 0
            for i, reading in enumerate(res):
                if (reading == -1):
                    weight += w_gauss(real_readings[i], 0)
                else:
                    weight += w_gauss(real_readings[i],self.distToIr(reading[0],
                                                                reading[1]))
            p.w = weight / 3.

        total_weight = sum([p.w for p in self.particles])
        if total_weight:
            for p in self.particles:
                p.w = p.w / total_weight


        weighted_dist = WeightedDistribution(self.particles)

        new_particles = []

        for _ in self.particles:
            new_particles.append(weighted_dist.pick())
            if new_particles[-1] is not None:
                can_pos = self.navmap.ConvertMMToCanvas(new_particles[-1].pos)
                pygame.draw.circle(canvas, (55,55,0),
                                   ((int)(can_pos[0])+8,(int)(can_pos[1])-8),
                                   (int)(50*new_particles[-1].w))

        pygame.display.update()

        particles = new_particles

        self.pos = self.computeMean()
