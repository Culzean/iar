__author__ = 's1578981'

import serial

class RobotEvent:

    def printName(self):
        print (self.name);

class ExploreStart(RobotEvent):

    def __init__(self):
        self.name = "Explore start";
        self.printName();

class ExploreEnd(RobotEvent):

    def __init__(self):
        self.name = "Explore start";
        self.printName();

class FollowWallStart(RobotEvent):

    def __init__(self):
        self.name = "Follow wall start";
        self.printName();

class FollowWallEnd(RobotEvent):

    def __init__(self):
        self.name = "Follow wall end";
        self.printName();

class EnteredDeadEnd(RobotEvent):

    def __init__(self):
        self.name = "Robot entered dead end";
        self.printName();

class FoundObstacle(RobotEvent):

    def __init__(self):
        self.name = "Robot has found something in its path";
        self.printName();

class GoHomeStart(RobotEvent):

    def __init__(self):
        self.name = "It's time to go home";
        self.printName();

class GoHomeEnd(RobotEvent):

    def __init__(self):
        self.name = "Gezzer has made it home";
        self.printName();

class SessionStart(RobotEvent):

    def __init__(self):
        self.name = "Start session"
        self.printName();

class SessionStop(RobotEvent):

    def __init__(self):
        self.name = "Robot Stop";
        self.printName();

class FoodFound(RobotEvent):

    def __init__(self):
        self.name = "Robot found food";
        self.printName();

class SearchForFood(RobotEvent):

    def __init__(self):
        self.name = "Gezzer will look for food";
        self.printName();

class GatherFood(RobotEvent):

    def __init__(self):
        self.name = "Gezzer will gather food";
        self.printName();

