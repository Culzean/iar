import numpy as np
import math
import Vec3
import time
import csv
import matplotlib.pyplot as plt
import pygame, sys
from pygame.locals import *

import SerialConnection
import RobotEvent
import RobotBehaviour
import NavMesh
import OdometryParticleFilter as pf

class robot:

    def __init__(self, message_system, outputfilename='path.png',
                 calibrate=False):
        self.connection = SerialConnection.SerialConnection()

        # CONSTANTS
        self.NAME = "Grezzer"

        self.GO_SPEED = 5
        self.STOP_SPEED = 0

        self.MAX_SPEED = 7

        self.LEFT_AIM = 70
        self.RIGHT_AIM = 70

        # Thresholds
        self.LTH_FRONT = 20
        self.HTH_FRONT = 30
        self.LTH_LEFT = 20
        self.HTH_LEFT = 30
        self.LTH_RIGHT = 40
        self.HTH_RIGHT = 60

        # IR sensor positions
        self.S_LEFT = slice(0,2)
        self.S_LEFT_BACK = 0
        self.S_LEFT_FRONT = 1
        self.S_FRONT = slice(2,4)
        self.S_FRONT_LEFT = 2
        self.S_FRONT_RIGHT = 3
        self.S_RIGHT = slice(4,6)
        self.S_RIGHT_FRONT = 4
        self.S_RIGHT_BACK = 5
        self.S_BACK_RIGHT = 6
        self.S_BACK_LEFT = 7


        self.NO_TURN = 0
        self.RIGHT_TURN = 1
        self.LEFT_TURN = -1

        self.RIGHT = 1
        self.LEFT = -1

        # Diameter in mm
        self.ROBOT_DIAMETER = 52
        self.startDir = 3.14

        # Navmesh
        self.navMesh = NavMesh.NavMesh(72,40, "navmap/robotmap.csv",outputfilename)

        # VARIABLES
        startpos = Vec3.vec3(690,130,self.startDir);
        #startpos = self.navMesh.ConvertGridPosToMM(Vec3.vec3(36,6,0))
        self.position = Vec3.vec3(startpos.x, startpos.y, self.startDir)
        self.odo_position = Vec3.vec3(startpos.x, startpos.y, self.startDir)
        self.homeLocation = Vec3.vec3(startpos.x, startpos.y, 0) #best remember this
        self.setOdometer(Vec3.vec3(0,0,0))

        # thresold to skip explore and switch to gather
        self.foodGatherThreshold = 1
        self.isHeadingHome = False
        self.crtGoal = None
        self.crtPath = []
        self.holdingFoodCount = 0

        #add a few test locations for food
        #self.AddFoodSource(Vec3.vec3(46, 6, 0))

        # Particle filter
        self.particle_filter = pf.OdometryParticleFilter(startpos, self.navMesh)

        self.left_speed = 0
        self.right_speed = 0

        self.going_home = False

        self.odometer = Vec3.vec3(0, 0, 0)

        self.calcodotime = time.time()

        self.prev_error = 0
        self.error = 0

        self.base_ir_output = np.zeros(8)

        # Calibrate or not
        if calibrate:
            print 'Calibrating IR sensors'
            self.calibrateIR()
        else:
            with open('base_ir.csv', 'rb') as csvfile:
                writer = csv.reader(csvfile, delimiter=',')
                try:
                    line = writer.next()
                    self.base_ir_output = np.array(map(float, line))
                except Exception, e:
                    print 'Error values in \'base_ir.csv\' wrong format:', e
                    print 'IR Output: ', self.base_ir_output

            if (len(self.base_ir_output) != 8):
                print 'Error wrong number of elements in \'base_ir.csv\': ',
                len(self.base_ir_output)
            if (self.base_ir_output.dtype != np.dtype('float64')):
                print 'Error wrong data type for elements in \'base_ir.csv\': ',
                self.base_ir_output.dtype


        self.hist_weigths = [1.240079365079365e-05, 9.92063492063492e-05,
                             0.0006944444444444445, 0.004166666666666667,
                             0.020833333333333332, 0.08333333333333333,
                             0.25, 0.5]

        self.ir_output = np.zeros(8)
        self.hist_ir_output = np.zeros([8,8])
        self.hist_weighted_ir_output = np.zeros(8)

        self.odometer_output = np.zeros(8)

        # Event system
        self.message_system = message_system
        self.crt_behaviour = RobotBehaviour.Explore(self.message_system)
        self.prev_behaviour = None
        self.setNewBehaviour(RobotBehaviour.Explore(self.message_system))
        self.message_system.robot = self
        #self.setNewBehaviour(RobotBehaviour.ArriveHome(self.message_system, 20.0))

        #self.message_system.sendMessage(RobotEvent.ExploreStart())

        #self.crtPath = self.navMesh.AStar((67, 32), (38, 10))

        self.facingVec = Vec3.vec3(np.cos(self.position.z),np.sin(self.position.z), self.startDir)

    def setOdometer(self, pos):
        output = self.connection.SendCommand("G," + str(pos.x) + "," +
                                             str(pos.y) + "\n")
        if output != 'g':
            print 'Serial error in setting the position counter'

    def lightLED(self):
        self.connection.SendCommand("L," + "0," + "2" + "\n")
        self.connection.SendCommand("L," + "1," + "2" + "\n")

    def calculatePos(self):
        if (isinstance(self.getOdometerOutput(), Vec3.vec3)):

            odometer = self.getOdometerOutput()

            velocity = (odometer - self.odometer)

            self.odo_position.x += 0.5 * (velocity.x + velocity.y) * \
            np.cos(self.odo_position.z)
            self.odo_position.y += 0.5 * (velocity.x + velocity.y) * \
            np.sin(self.odo_position.z)
            self.odo_position.z -= (velocity.x - velocity.y) / \
            (self.ROBOT_DIAMETER)

            self.odometer = odometer

    def move(self):
        max(min(self.right_speed, self.MAX_SPEED), -self.MAX_SPEED)
        max(min(self.left_speed, self.MAX_SPEED), -self.MAX_SPEED)
        output = self.connection.SendCommand("D," + str(self.left_speed) + \
                                             "," + str(self.right_speed) + "\n")

        if output != 'd':
            print 'Serial error in setting the speed: ', output

    def setSpeed(self, left_speed, right_speed):
        self.left_speed = left_speed
        self.right_speed = right_speed

    def calibrateIR(self, read_iterations=50):
        i = read_iterations
        while (i > 0):
            output = self.connection.SendCommand("N\n")

            try:
                ir_output = output[2:].split(',')
                self.base_ir_output += np.array(map(float, ir_output))

                i -= 1

            except Exception, e:
                print 'Error IR Output wrong format: ', e
                print 'IR Output: ', output

        self.base_ir_output /= float(read_iterations)

        with open('base_ir.csv', 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter=',')
            writer.writerow(map(str, self.base_ir_output))


    def readIR(self):
        output = self.connection.SendCommand("N\n")

        try:
            ir_output = output[2:].split(',')

            if (len(ir_output) == 8):
                self.ir_output = np.array(map(int, ir_output))
            else:
                raise('Wrong number of elements in IR reading')

        except Exception, e:
            print 'Error IR Output wrong format: ', e
            print 'IR Output: ', output
            self.ir_output = None

    def getIRSensorOutput(self):
        if self.ir_output is not None:
            # Calibrate IR Sensor
            calibrated_ir_output = np.clip(self.ir_output -
                                           self.base_ir_output, 0, 1000)
            for i, value in enumerate(calibrated_ir_output):
                self.hist_ir_output[i][0] = value
                self.hist_ir_output[i] = np.roll(self.hist_ir_output[i], -1)
                # Take weighted history values
                self.hist_weighted_ir_output[i] = np.around(np.sum([w*x for w,x in
                                                  zip(self.hist_weigths,
                                                      self.hist_ir_output[i])]),2)

            return self.hist_weighted_ir_output

        return None

    def readOdometer(self):
        odometer_output = self.connection.SendCommand("H\n")

        try:
            odometer_output = odometer_output[2:].split(',')
            self.odometer_output = np.array(map(int, odometer_output))
        except:
            self.odometer_output = None
            print 'Odometer Output wrong format'

    def getOdometerOutput(self):
        if self.odometer_output is not None:
            return 0.08*Vec3.vec3(self.odometer_output[0],
                                  self.odometer_output[1], 0)

        return None

    def Draw(self, canvas):
        pos = self.navMesh.ConvertMMToCanvas(self.position)
        self.position = self.particle_filter.pos
        halfWidth = self.navMesh.TILESIZE / 2

        pygame.draw.circle(canvas, self.navMesh.GREEN, ((int)(pos[0])+8,(int)(pos[1])-8), (int)(halfWidth*2.2))

        temp = self.facingVec
        temp.y = temp.y * -1
        temp = temp * 20
        endPos = (pos + temp)
        pygame.draw.line(canvas, (0,0,33), (pos[0]+8,pos[1]-8), (endPos[0]+8, endPos[1]-8) )
        pygame.display.update()

    def update(self, dt, canvas):
        if self.crtGoal is not None:
            print self.crtGoal
        self.readIR()
        self.readOdometer()
        old_pos = Vec3.vec3(self.odo_position.x, self.odo_position.y, self.odo_position.z)

        self.calculatePos()

        dpos = self.odo_position - old_pos
        sreading = self.getIRSensorOutput()
        print dpos.z
        if sreading is not None:
            self.particle_filter.update(dpos.x, dpos.y, dpos.z,
                                    np.average(sreading[self.S_FRONT]),
                                    np.average(sreading[self.S_LEFT]),
                                    np.average(sreading[self.S_RIGHT]),
                                    canvas)
        self.position = self.particle_filter.pos
        self.facingVec = Vec3.vec3(np.cos(self.position.z),np.sin(self.position.z), 0)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w:
                    print('Forward')
                elif event.key == pygame.K_s:
                    print('Backward')

        self.crt_behaviour.update(self, dt)
        self.move()

    def SelectFoodSource(self):
        if len(self.navMesh.foodLocations) <= 0:
            return None
        else:
            return self.navMesh.foodLocations[0] #return shortest

    def AddFoodSource(self, mapLocation):
        self.lightLED()
        self.navMesh.AddFoodSource(mapLocation)

        print "Found food at location ", mapLocation

    def robotEvent(self, _event):

        if isinstance(_event, RobotEvent.FoundObstacle):
            avoiding = RobotBehaviour.Avoid(self.message_system)
            self.setNewBehaviour(avoiding)

        elif isinstance(_event, RobotEvent.ExploreStart):

            explore = RobotBehaviour.Explore(self.message_system)
            self.setNewBehaviour(explore)

        elif isinstance(_event, RobotEvent.ExploreEnd):

            print "Grezzer will continue following path"
            pathToCell = RobotBehaviour.FollowPath(self.message_system, self.crtGoal)
            self.setNewBehaviour(pathToCell)

        elif isinstance(_event, RobotEvent.FollowWallStart):
            following = RobotBehaviour.FollowWallPID(self.message_system)
            self.setNewBehaviour(following)

        elif isinstance(_event, RobotEvent.SessionStop):
            stop = RobotBehaviour.Stop(self.message_system)
            self.setNewBehaviour(stop)

        elif isinstance(_event, RobotEvent.GoHomeStart):
            self.isHeadingHome = True;
            print "Going back to home ", self.homeLocation
            homeCell = self.navMesh.ConvertPosToCell(self.homeLocation)
            pathToHome = RobotBehaviour.FollowPath(self.message_system, homeCell)
            self.setNewBehaviour(pathToHome)

        elif isinstance(_event, RobotEvent.FoodFound):
            foodfound = RobotBehaviour.FoodFound(self.message_system, 2.3)
            self.setNewBehaviour(foodfound)

        elif isinstance(_event, RobotEvent.GatherFood):
            foodCell = self.SelectFoodSource()
            #foodCell = self.navMesh.ConvertPosToCell(foodCell)
            goToFood = RobotBehaviour.FollowPath(self.message_system, foodCell)
            self.setNewBehaviour(goToFood)

        elif isinstance(_event, RobotEvent.GoHomeEnd):
            homeEvent = RobotBehaviour.ArriveHome(self.message_system, 2.3)
            self.setNewBehaviour(homeEvent)

        elif isinstance(_event, RobotEvent.SearchForFood):
            pass
            #foodfound = RobotBehaviour.FoodFound(self.message_system, 2.3)
            ##self.setNewBehaviour(foodfound)

    def setNewBehaviour(self, newBehaviour):
        self.prev_behaviour = self.crt_behaviour
        self.crt_behaviour = newBehaviour
        print "\n-- Change in behaviour --\n" + \
            self.prev_behaviour.name + " ==> " + self.crt_behaviour.name
        self.crt_behaviour.start()
        self.prev_behaviour.reset()
